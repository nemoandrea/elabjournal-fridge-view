
// manage the user input values

const api_input = document.getElementById('api_key_input');
const storage_input = document.getElementById('storageLayerID_input');
api_input.addEventListener('blur', save_local_values);
storage_input.addEventListener('blur', save_local_values);

// Check if there's saved text on page load
const savedApiKey = localStorage.getItem('elab-api-key');
if (savedApiKey) {
    api_input.value = savedApiKey;
}
const savedFridgeId = localStorage.getItem('elab-fridge-layer-id');
if (savedFridgeId) {
    storage_input.value = savedFridgeId;
}

// update (save to local storage) the API key and storageLayerID of fridge
function save_local_values() {
    const key = api_input.value;
    localStorage.setItem('elab-api-key', key);
    const layerID = storage_input.value;
    localStorage.setItem('elab-fridge-layer-id', layerID);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

// const apiButton = document.getElementById('api-check');
// apiButton.addEventListener('click', test_API);

// function test_API() {
//     get_elab_json("storage")
// }
/////////////////////

const update_button = document.getElementById('getdata');
update_button.addEventListener('click', get_fridge_info);

async function get_elab_json(url_extension) {
    //eLabJournal API
    //token can be generated through web interface tudelft.elabournal.com  
    const baseurl = "https://tudelft.elabjournal.com/api/v1/";
    key = localStorage.getItem('elab-api-key');
    //console.log("using api key: " + key);
    url = baseurl+url_extension;
    //console.log(url);

    response = await fetch(url, {
        method: "get",
        headers: {
            "accept": "application/json",
            "Authorization": key
        }
    });

    //console.log(response.status)
    parsed_response = await response.json()
//    console.log(parsed_response)
    return parsed_response
}


function list_storage_IDs(){
    console.log("Listing available storage devices (including non-storage equipment):")
    content = get_elab_json("storage")
    console.log(content.data)
    for (storage of content.data){
        console.log(`- Storage Device '${storage.name}' with storageLayerID ${storage.storageLayerID}`)
    }

}


async function get_fridge_info(depth=4){
    storageLayerID = localStorage.getItem('elab-fridge-layer-id');

    fridge = await parse_fridge(storageLayerID)

    console.log(fridge)

    if (fridge.size !== 0) {
        console.log("done loading")

        // hide the error message
        var errorMonkey = document.getElementById("error-message");
        errorMonkey.style.display = "none"

        // and show the fridge data
        build_fridge_html(fridge)
  
    } else {
        console.log("ya fucked")
    }

    return fridge
}


async function parse_fridge(parentLayerID) {
    const fridgeobj = await get_elab_json(`storageLayers/${storageLayerID}`)
    console.log(`Parsing information about fridge '${fridgeobj.name}' ('${storageLayerID}')`)
    
    fridge = new Object()  // define a new object for our fridge
    fridge.name = fridgeobj.name
    fridge.drawers = []  // drawers are stored as array, where each drawers contains n boxes each with
                         // information about its own compartment, row- and colindex and position in a drawer.       

    compartments = (await get_elab_json(`storageLayers/${parentLayerID}/childLayers`)).data
    fridge.num_compartments = Object.keys(compartments).length

    for ([compidx, compartment] of compartments.entries()) {
        columns = (await get_elab_json(`storageLayers/${compartment.storageLayerID}/childLayers`)).data
        fridge.num_columns = Object.keys(columns).length

        for ([colidx, column] of columns.entries()) {
            rows = (await get_elab_json(`storageLayers/${column.storageLayerID}/childLayers`)).data
            fridge.num_rows = Object.keys(rows).length

            for ([rowidx, row] of rows.entries()) {
                boxes = (await get_elab_json(`storageLayers/${row.storageLayerID}/childLayers`)).data

                drawer = []
                boxes.forEach((boxObj, idx) => {
                    box = new Object()
                    box.storageLayerID = boxObj.storageLayerID
                    box.name = boxObj.name
                    box.comparment = compidx
                    box.column = colidx
                    box.row = rowidx
                    box.position = idx
                    drawer.push(box)
                }) 
                fridge.drawers.push(drawer)
            }
        }
    }
    return fridge
}


function build_fridge_html(fridge) {
    var fridgeEl = document.getElementById("fridge");
    fridgeEl.replaceChildren();  // clear all previously added divs

    // configure the grid settings
    fridgeEl.style.gridTemplateRows = `2em repeat(${fridge.num_rows*fridge.num_compartments}, 1fr)`
    fridgeEl.style.gridTemplateColumns = `repeat(${fridge.num_cols}, 1fr)`

    // add a simple header to the grid
    let fridgeHeaderEl = document.createElement('div');
    fridgeHeaderEl.textContent = fridge.name
    fridgeHeaderEl.className = "fridge-header"
    fridgeHeaderEl.style.gridArea = "1 / 1 / 1 / -1"
    fridgeEl.appendChild(fridgeHeaderEl)

    // add the boxes 
    console.log("populating fridge display ")
    fridge.drawers.forEach((drawer) => {
        let drawerEl = document.createElement('div');
        drawerEl.className = "fridge-drawer"

        for (box of drawer) {
            const rowstart = (box.row + box.comparment*fridge.num_rows)  + 2
            const colstart = box.column + 1
            drawerEl.style.gridArea = `${rowstart} / ${colstart}`  // note that this is drawerEl

            let boxEl = document.createElement('div');
            boxEl.className = "fridge-box"        
            boxEl.textContent = box.name
            drawerEl.appendChild(boxEl)
        }
        fridgeEl.appendChild(drawerEl)
        
    })
}

